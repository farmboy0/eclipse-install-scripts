#!/bin/bash

#set -x 

destArg=""
if [ "$1" != "" ];
then
	destArg="-destination "$1
fi

declare -A repos

while read -u 10 repoName repoUrl; do
	repos["$repoName"]="$repoUrl"
done 10< ./eclipse.repositories

grep -v '^[ ]*$\|^[ ]*#' eclipse.features | while read site feature; do
	echo -e "\n\n$site -> ${feature//[[:space:]]/}"

	for repoName in "${!repos[@]}"
	do
		site=${site/$repoName/${repos[$repoName]}}
	done

	./eclipse/eclipse -application org.eclipse.equinox.p2.director -noSplash -r "$site" -i "${feature//[[:space:]]/}" $destArg
done

