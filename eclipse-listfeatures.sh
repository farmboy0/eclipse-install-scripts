#!/bin/sh

if [ "$1" == "" ];
then
	echo "run with $0 <repository-name>"
	exit 1
fi

declare -A repos

while read -u 10 repoName repoUrl; do
        repos["$repoName"]="$repoUrl"
done 10< ./eclipse.repositories

if [ "${repos["$1"]}" == "" ];
then
	echo "unknown repo name $1"
	exit 1
fi

./eclipse/eclipse -application org.eclipse.equinox.p2.director -noSplash -l -repository "${repos["$1"]}" | grep feature.group | grep -v .source | sed -e 's/=.*//g' | uniq
